from torch_geometric_temporal.nn.recurrent import EvolveGCNH
from lib.data import PYGTDataset
from lib.rgcnn import RGCNNE


dataset = PYGTDataset()
model = RGCNNE(dataset.topology, dataset.feature_count,
               lambda: EvolveGCNH(
                  dataset.node_count,
                  dataset.feature_count,
                  add_self_loops=False))
model.fit(dataset)
