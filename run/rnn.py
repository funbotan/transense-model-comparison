from lib.data import SQL2NN
from lib.rnn import RNN


data = SQL2NN()
model = RNN(data.num_nodes)
model = model.to(model.device)
data.device = model.device
model.fit(data)
