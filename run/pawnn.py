from lib.data import SQL2NN
from lib.pawnn import PAWNN


data = SQL2NN()
model = PAWNN(data.num_nodes, data.topology)
model = model.to(model.device)
data.device = model.device
model.fit(data)
