from torch_geometric_temporal.nn.recurrent import EvolveGCNO
from lib.data import PYGTDataset
from lib.rgcnn import RGCNNE


dataset = PYGTDataset()
model = RGCNNE(dataset.topology, dataset.feature_count,
               lambda: EvolveGCNO(dataset.feature_count))
model.fit(dataset)
