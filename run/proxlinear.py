from lib.data import SQL2NN
from lib.proxlinear import PLN


data = SQL2NN()
model = PLN(2 * data.num_nodes, 2 * data.num_nodes)
model = model.to(model.device)
data.device = model.device
model.fit(data)
