from torch_geometric_temporal.nn.recurrent import TGCN
from lib.data import PYGTDataset
from lib.rgcnn import RGCNNT


dataset = PYGTDataset()
model = RGCNNT(dataset.topology, dataset.feature_count,
               lambda: TGCN(dataset.feature_count, dataset.feature_count))
model.fit(dataset)
