from typing import Callable
from tqdm import tqdm
import networkx as nx
import torch
import torch.nn.functional as F
from torch_geometric_temporal.signal import temporal_signal_split
from statistics import fmean
from lib.misc import max_diam_vcp, plot_error
from lib.base import BaseModel


class RGCNN(BaseModel):
    def __init__(
        self,
        topology: nx.Graph,
        num_features: int,
        layer: Callable[..., torch.nn.Module]
    ):
        super(RGCNN, self).__init__()
        diam = max_diam_vcp(topology)
        self.num_features = num_features
        self.topology = topology
        self.layers = torch.nn.ModuleList(layer() for _ in range(diam))
        self.out_layer = torch.nn.Linear(num_features, num_features)
        self.optimizer = torch.optim.Adam(self.parameters())

    def fit(self, dataset, train_ratio=0.5, epochs=1):
        train_dataset, test_dataset = \
            temporal_signal_split(dataset, train_ratio)
        loss_plot = []
        for epoch in range(epochs):
            # Train
            for snapshot in tqdm(train_dataset):
                y_hat = self.forward(snapshot.x,
                                     snapshot.edge_index)
                loss = F.mse_loss(y_hat, snapshot.y)
                loss.backward()
                self.optimizer.step()
                self.optimizer.zero_grad()
                loss_plot.append(loss.detach())
        ttsp = len(loss_plot)
        # Evaluate
        with torch.no_grad():
            vertex_loss = 0
            for snapshot in test_dataset:
                y_hat = self.forward(snapshot.x,
                                     snapshot.edge_index)
                loss = F.mse_loss(y_hat, snapshot.y)
                loss_plot.append(loss)
                vertex_loss += torch.abs(y_hat - snapshot.y)
            vertex_loss = torch.linalg.norm(vertex_loss, axis=1)
            vertex_loss /= len(loss_plot) - ttsp
            # Display metrics
            plot_error(self.topology, loss_plot, vertex_loss, ttsp)
        print("MSE: {:.4f}".format(fmean(loss_plot[ttsp:])))
        print(str(self.count_params()), "parameters")


class RGCNNE(RGCNN):
    def __init__(self, *args, **kwargs):
        super(RGCNNE, self).__init__(*args, **kwargs)

    def forward(self, x, edge_index):
        for layer in self.layers:
            x = layer(x, edge_index)
        x = F.relu(x)
        x = self.out_layer(x)
        return x


class RGCNNT(RGCNN):
    def __init__(self, *args, **kwargs):
        super(RGCNNT, self).__init__(*args, **kwargs)
        self.h = torch.randn(len(self.topology.nodes()),
                             self.num_features)

    def forward(self, x, edge_index):
        h = self.h
        for layer in self.layers:
            h = layer(x, edge_index, H=h)
        self.h = h.detach()
        h = self.out_layer(h)
        return h
