import torch
from lib.base import BaseModel


class RNN(BaseModel):
    def __init__(self, features: int):
        super().__init__()
        features *= 2
        self.model = torch.nn.RNN(
            features, features, batch_first=True)
        self.loss = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.parameters())
        self.h = torch.randn(1, features).to(self.device)

    def forward(self, x):
        x = torch.unsqueeze(x, 0)
        y, h = self.model(x, self.h)
        self.h = h.detach()
        return torch.squeeze(y, 0)
