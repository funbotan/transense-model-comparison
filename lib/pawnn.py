import networkx as nx
import numpy as np
import torch
import torch.nn.utils.prune as prune
from lib.misc import max_diam_vcp
from lib.base import BaseModel


class PAWNN(BaseModel):
    def __init__(
        self,
        features: int,
        topology: nx.Graph
    ):
        super().__init__()
        # The dimensionality of hidden layers =
        # = number of nodes in the grid
        features *= 2
        # Graph diameter determines the depth of the network
        self.D = max_diam_vcp(topology)
        # Adjacency matrix will be used to prune the hidden layers
        self.M = nx.adjacency_matrix(topology).todense()
        self.M += np.identity(self.M.shape[0], dtype=np.int32)
        self.M = np.repeat(self.M, 2, axis=0)
        self.M = np.repeat(self.M, 2, axis=1)
        self.M = torch.tensor(self.M, dtype=torch.bool)
        # Input layer -> (Hidden layer -> Prune) * D -> Output layer
        self.hidden_layers = torch.nn.ModuleList()
        for _ in range(self.D):
            layer = torch.nn.Linear(features, features, dtype=torch.float)
            layer = prune.custom_from_mask(layer, "weight", self.M)
            self.hidden_layers.append(layer)
        self.out_layer = torch.nn.Linear(features, features, dtype=torch.float)
        # Training objects
        self.loss = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.parameters())

    def forward(self, x):
        for layer in self.hidden_layers:
            x = layer(x)
            x = torch.tanh(x)
        x = self.out_layer(x)
        return x
