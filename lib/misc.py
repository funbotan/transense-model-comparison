import matplotlib.pyplot as plt
import pandas as pd
import networkx as nx
from scipy.ndimage.filters import uniform_filter1d


def max_diam_vcp(topology: nx.Graph):
    """
    Maximum diameter of vertex cut partition.
    What that means:
    We split the graph along its articulation points,
    calculate the diameters of resulting subgraphs
    and return the max of them.
    """
    v = set(nx.articulation_points(topology)) & topology.observable_nodes
    parts = nx.Graph(topology)
    parts.remove_nodes_from(v)
    parts = nx.connected_components(parts)
    parts = map(topology.subgraph, parts)
    return max(map(nx.diameter, parts))


def plot_error(topology, loss_plot, vertex_loss, ttsp):
    plt.subplot(121)
    loss_plot = uniform_filter1d(loss_plot, size=100)
    plt.plot(loss_plot[:ttsp], label='train')
    plt.plot(range(ttsp, len(loss_plot)),
             loss_plot[ttsp:], label='test')
    plt.legend()
    plt.xlabel("Number of data points")
    plt.ylabel("Mean squared error")
    plt.title("Convergence graph")
    plt.grid()
    plt.subplot(122)
    df = pd.DataFrame(index=topology.nodes(),
                      columns=topology.nodes())
    for row, data in nx.shortest_path_length(topology):
        for col, dist in data.items():
            df.loc[row, col] = max(dist, 10)
    pos = nx.kamada_kawai_layout(topology,
                                 dist=df.to_dict())
    ec = nx.draw_networkx_edges(topology, pos,
                                alpha=0.2)
    nc = nx.draw_networkx_nodes(topology, pos,
                                nodelist=topology.nodes(),
                                node_color=vertex_loss,
                                node_size=10,
                                cmap=plt.cm.jet)
    cbar = plt.colorbar(nc)
    cbar.ax.set_ylabel("Absolute error",
                       rotation=270, labelpad=15)
    plt.title("Per-node error")
    plt.show()
