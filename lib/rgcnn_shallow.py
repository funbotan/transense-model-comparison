from typing import Callable
from tqdm import tqdm
import networkx as nx
import torch
import torch.nn.functional as F
from torch_geometric_temporal.signal import temporal_signal_split
from statistics import fmean
from lib.misc import plot_error
from lib.base import BaseModel


class RGCNN(BaseModel):
    def __init__(
        self,
        topology: nx.Graph,
        num_features: int,
        layer: Callable[..., torch.nn.Module]
    ):
        super(RGCNN, self).__init__()
        self.num_features = num_features
        self.topology = topology
        self.layer = layer()
        self.out_layer = torch.nn.Linear(num_features, num_features)
        self.optimizer = torch.optim.Adam(self.parameters())

    def fit(self, dataset: object, train_ratio: object = 0.5, epochs: object = 1) -> object:
        train_dataset, test_dataset = \
            temporal_signal_split(dataset, train_ratio)
        loss_plot = []
        for epoch in range(epochs):
            # Train
            for snapshot in tqdm(train_dataset):
                y_hat = self.forward(snapshot.x,
                                     snapshot.edge_index)
                loss = F.mse_loss(y_hat, snapshot.y)
                loss.backward()
                self.optimizer.step()
                self.optimizer.zero_grad()
                loss_plot.append(loss.detach())
        ttsp = len(loss_plot)
        # Evaluate
        with torch.no_grad():
            vertex_loss = 0
            for snapshot in test_dataset:
                y_hat = self.forward(snapshot.x,
                                     snapshot.edge_index)
                loss = F.mse_loss(y_hat, snapshot.y)
                loss_plot.append(loss)
                vertex_loss += torch.abs(y_hat - snapshot.y)
            vertex_loss = torch.linalg.norm(vertex_loss, axis=1)
            vertex_loss /= len(loss_plot) - ttsp
            # Display metrics
            plot_error(self.topology, loss_plot, vertex_loss, ttsp)
        print("MSE: {:.4f}".format(fmean(loss_plot[ttsp:])))
        print(str(self.count_params()), "parameters")


class RGCNNE(RGCNN):
    def __init__(self, *args, **kwargs):
        super(RGCNNE, self).__init__(*args, **kwargs)

    def forward(self, x1, edge_index):
        num_iterations = 0
        x0 = torch.zeros(x1.shape)
        while F.l1_loss(x0, x1) > 1e-3:
            x0 = x1
            x1 = self.layer(x1, edge_index)
            num_iterations += 1
        x = F.relu(x1)
        x = self.out_layer(x)
        print(str(num_iterations))
        return x


class RGCNNT(RGCNN):
    def __init__(self, *args, **kwargs):
        super(RGCNNT, self).__init__(*args, **kwargs)
        self.h = torch.randn(len(self.topology.nodes()),
                             self.num_features)

    def forward(self, x, edge_index):
        h = self.h
        for _ in range(self.diam):
            h = self.layer(x, edge_index, H=h)
        self.h = h.detach()
        h = F.relu(h)
        h = self.out_layer(h)
        return h
