import cmath
import random
import numpy as np
import networkx as nx
import pandas as pd
import sqlalchemy as sql
import torch
from torch.utils.data import Subset
import torch_geometric as pyg
import torch_geometric_temporal as pygt
from lib.misc import max_diam_vcp


class SQL2NN(torch.utils.data.Dataset):
    def __init__(
            self,
            iosplit=0.8,
            measure_articulation_points=True,
            url="postgresql://postgres:password@localhost:5432/postgres"
    ):
        super().__init__()
        random.seed(12345)
        self.device = 'cpu'
        # Download data from the database
        engine = sql.create_engine(url)
        engine.dialect.description_encoding = None
        with engine.connect() as conn:
            nodes = pd.read_sql("SELECT uuid FROM input.node_input", conn)
            lines = pd.read_sql(
                "SELECT uuid, node_a, node_b "
                "FROM input.line_input UNION "
                "SELECT uuid, node_a, node_b "
                "FROM input.switch_input "
                "WHERE input.switch_input.closed UNION "
                "SELECT uuid, node_a, node_b "
                "FROM input.transformer_2_w_input",
                conn,
            )
            result = pd.read_sql(
                "SELECT input_model, time, v_mag, v_ang "
                "FROM result.node_res",
                conn,
            )
        # Convert input model into a graph
        self.topology = nx.OrderedGraph()
        self.topology.add_nodes_from(nodes["uuid"])
        self.topology.add_edges_from(zip(lines["node_a"], lines["node_b"]))
        # Convert result model into a (complex-valued) dataframe
        result['value'] = result.apply(
            lambda x: cmath.rect(x['v_mag'], x['v_ang']), axis=1)
        result.drop_duplicates(inplace=True)
        result = result.pivot(index='time',
                              columns='input_model',
                              values='value')
        self.num_timesteps, self.num_nodes = result.shape
        # "Truth" stands for ground truth
        self.truth = result.reindex(self.topology.nodes, axis=1)
        assert self.truth.notna().values.all()
        # "Lie" stands for data sampled from gaussian noise based on
        # ground truth
        means = self.truth.mean(axis=0)
        covs = [np.cov(np.real(self.truth[feature]),
                       np.imag(self.truth[feature]))
                for feature in self.truth]
        self.lie = [np.squeeze(
                     np.random.multivariate_normal(
                      [mean.real, mean.imag], cov, len(result)
                     ).view(np.complex))
                    for mean, cov in zip(means, covs)]
        self.lie = pd.DataFrame(self.lie).T
        self.lie = pd.DataFrame(data=self.lie.values,
                                columns=self.truth.columns,
                                index=self.truth.index)
        assert self.truth.shape == self.lie.shape
        # It would be nice to know where the measurement devices are
        # actually located, but for now we'll just randomize them
        if measure_articulation_points:
            itruths = [self.truth.columns.get_loc(ap)
                       for ap in nx.articulation_points(self.topology)]
            diff = int(self.num_nodes * iosplit) - len(itruths)
            if diff > 0:
                itruths += random.sample(range(self.num_nodes), diff)
            else:
                itruths = [x for x in itruths if x not in
                           random.sample(itruths, abs(diff))]
        else:
            itruths = random.sample(
                range(self.num_nodes),
                int(self.num_nodes * iosplit))
        ilies = list(filter(lambda x: x not in itruths, range(self.num_nodes)))
        self.input_data = pd.concat(
            [self.truth.iloc[:, itruths], self.lie.iloc[:, ilies]], axis=1)
        self.input_data = self.input_data[self.truth.columns]
        assert self.truth.shape == self.input_data.shape
        self.topology.observable_nodes = set(self.truth.columns[itruths])
        print("Max diam VCP is", str(max_diam_vcp(self.topology)))

    def __len__(self):
        return len(self.truth)

    def __getitem__(self, idx):
        input = self.input_data.iloc[idx].values.copy().view(np.float)
        output = self.truth.iloc[idx].values.copy().view(np.float)
        input = torch.tensor(input, dtype=torch.float32)
        output = torch.tensor(output, dtype=torch.float32)
        return input.to(self.device), output.to(self.device)

    def train_test_split(self, split):
        train_size = int(split * len(self))
        train = Subset(self, np.arange(train_size))
        test = Subset(self, np.arange(train_size, len(self)))
        return train, test


def PYGTDataset():
    query = SQL2NN()
    graph = pyg.utils.from_networkx(query.topology)
    input = np.dstack((query.input_data.values.real,
                       query.input_data.values.imag))
    output = np.dstack((query.truth.values.real,
                        query.truth.values.imag))
    dataset = pygt.StaticGraphTemporalSignal(
        graph.edge_index,
        graph.edge_attr,
        list(input),
        list(output))
    dataset.node_count = len(query.topology.nodes)
    dataset.feature_count = 2
    dataset.timesteps_count = len(query)
    dataset.topology = query.topology
    return dataset
