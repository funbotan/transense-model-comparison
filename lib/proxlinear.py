import torch
from lib.base import BaseModel


class PLN(BaseModel):
    def __init__(self, input_dim: int, output_dim: int):
        super().__init__()
        self.model = torch.nn.Sequential(
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, input_dim),
            torch.nn.ReLU(),
            torch.nn.Linear(input_dim, output_dim),
        )
        self.loss = torch.nn.HuberLoss()
        self.optimizer = torch.optim.Adam(self.parameters())

    def forward(self, x):
        return self.model(x)
