from abc import abstractmethod, ABC
from tqdm import tqdm
import torch
import torch.nn.functional as F
from statistics import fmean
from lib.data import SQL2NN
from lib.misc import plot_error


class BaseModel(torch.nn.Module, ABC):
    def __init__(self):
        super().__init__()
        torch.autograd.set_detect_anomaly(True)
        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        self.loss = None
        self.optimizer = None

    def train_step(self, x, y):
        self.optimizer.zero_grad()
        z = self(x)
        loss = self.loss(z, y)
        loss.backward()
        self.optimizer.step()
        mse = F.mse_loss(z, y)
        return mse.cpu()

    def eval_step(self, x, y):
        z = self(x)
        mse = F.mse_loss(z, y)
        vertex = torch.abs(z - y)
        return mse.cpu(), vertex.cpu()

    def count_params(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

    def fit(self, dataset: SQL2NN, train_ratio=0.5, epochs=1):
        train_data, test_data = dataset.train_test_split(train_ratio)
        loss = []
        vertex_loss = 0
        for epoch in range(epochs):
            for x, y in tqdm(train_data):
                loss.append(self.train_step(x, y))
        ttsp = len(loss)
        with torch.no_grad():
            for x, y in test_data:
                l, v = self.eval_step(x, y)
                loss.append(l)
                vertex_loss += v
            vertex_loss = vertex_loss.view(vertex_loss.shape[0]//2, 2)
            vertex_loss = torch.linalg.norm(vertex_loss, axis=1)
            vertex_loss /= len(loss) - ttsp
            plot_error(dataset.topology, loss, vertex_loss, ttsp)
        print("MSE: {:.4f}".format(fmean(loss[ttsp:])))
        print(str(self.count_params()), "parameters")
