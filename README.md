# TRANSENSE Model Comparison Paper

This repository contains the data and source code for the paper *Application of Recurrent Graph Convolutional Networks to the Neural State Estimation problem* submitted to NeurIPS.

## Installation

To run the experiments, two things need to be set up:

    1. A docker container hosting the dataset
    2. A python environment to run the code

You need to have Docker installed and configured on your machine. Also, CUDA support is strongly recommended, although the training algorithm should automatically default to CPU if CUDA is not supported.

The command below will download the image and start the container.
This process includes populating the database, which takes quite some time and needs to be done every time you boot up the container since the database is stored inside the container.
But you can provide a Docker volume to make the second start much faster.
Replace `/absolute/path/to/folder` with any absolute path on your system.

```bash
docker run -dp 5432:5432 -v /absolute/path/to/folder:/var/lib/postgresql/data registry.gitlab.com/funbotan/transense-model-comparison/training-data
```

If you want to use another port or run the container on a different host entirely, make corresponding changes to the database connection string in `lib/data.py` at line 19.

Next, the python environment needs to be set up.
Follow the [installation instructions for conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) to install (mini)conda on your system.
Then download or clone [the repository](https://gitlab.com/funbotan/transense-model-comparison):

```bash
git clone https://gitlab.com/funbotan/transense-model-comparison.git
```

Let conda install the environment for you:

```bash
cd transense-model-comparison
conda env create -f environment.yml -n transense_model_comparison
```

Once installed, the conda environment needs to be activated:

```bash
conda activate transense_model_comparison
```

## Usage

By now, you should be able to run the experiments. 
All executable files are located in the folder `run`, one for each model.
Choose one and execute (from the root directory of the repository), e.g.,

```bash
python -m run.evolvegcnh
```

to run the EvolveGCN-H model experiment.
This will take some time.
Once the training process is finished, you will find two values in the output of the console: MSE and the number of parameters. It should also display a plot representing convergence and per-node error.

To reproduce a [specific result](https://gitlab.com/funbotan/transense-model-comparison/-/blob/master/results.xlsx), uncomment line 22 in `lib/data.py` and insert a corresponding random seed.

